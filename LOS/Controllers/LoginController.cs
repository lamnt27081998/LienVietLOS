﻿using Microsoft.AspNetCore.Mvc;

namespace LOS.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login()
        {
            return View();
        }

        public IActionResult ActionLogin(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || !(username.Equals("admin") && password.Equals("admin")))
            {
                return Json(new { code = "FAILED", message = "Đăng nhập thất bại" });
            }
            return RedirectToAction("Index", "Home");
        }
    }
}
